<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
// Start the session
session_start();
?>
<html>
    <head>
        <title>Comentarios - AniTechVG</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />		
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="anime tecnología videojuegos reseñas" />
        <meta name="description" content="Página dedicada a la creación de contenido editorial de Anime, Tecnología y Videojuegos" />
        <meta name="author" content="AniTechVg"/>
        <link rel="stylesheet" type="text/css" href="../css/reset.css"/>
        <link rel="stylesheet" type="text/css" href="../css/style.css"/>        
        <link rel="stylesheet" type="text/css" href="../css/bjqs.css"/>
        <link rel="stylesheet" type="text/css" href="../css/demo.css"/>
        <script src="../js/main.js"></script> <!-- Gem jQuery -->
        <script src="../js/jquery-1.12.4.min.js"></script>
        <script src="../js/bjqs-1.3.min.js"></script>
    </head>
    <body>        

        <div id ="navbar">
            <ul class="topnav">
                <li><a href="#home" class="active"><img id="logo-nav" src="../images/logofinal2.jpg"/></a></li>
                <li><a href="http://localhost:11288/Default.aspx?tipo=3">Noticias</a></li>
                <li><a href="http://localhost:11288/Default.aspx?tipo=1">Reviews</a></li>
                <li><a href="http://localhost:11288/Default.aspx?tipo=2">Previews</a></li>
                <li><a href="http://localhost:11288/Default.aspx?tipo=4">Features</a></li>
                <?php
                if (isset($_SESSION['sid'])) {
                    ?>
                    <li><a href="http://localhost:8080/AniTechVg/usuario/index.jsp">Bienvenid@<?= $_SESSION['usn'] ?></a></li>
                    <li><a href="http://localhost:8080/AniTechVg/usuario/logout.jsp">Logout</a></li>
                    <?php
                } else {
                    ?>
                    <li><a href="http://localhost:8080/AniTechVg/index.jsp">Login</a></li>
                    <?php
                }
                ?>     
                <li class="icon"><a href="javascript:void(0);" onclick="myFunction()">&#9776;</a></li>
            </ul>
        </div>
        <div id="slider" style="background-image: url(../images/pattern.jpg); width: 100%;">
            <div class = "row">
                <div class = "col-4 col-mm-3">				
                </div>
                <div class = "col-4 col-mm-6 col-m-12">
                    <div id="banner-fade">

                        <!-- start Basic Jquery Slider -->
                        <ul class="bjqs">
                            <li><a href ="http://localhost:11288/2016/06/06/se-disparan-los-rumores-de-nuevos-contenidos-para-mario-kart-8/Default.aspx"><img src="../images/img1.jpg" title="Se disparan los rumores de nuevo contenido para MK8"></a></li>
                            <li><a href ="http://localhost:11288/2016/06/06/gamestop-espera-que-se-anuncien-nuevas-consolas-en-e3-2016/Default.aspx"><img src="../images/img2.jpg" title="Se esperan nuevas consolas para este año"></a></li>
                            <li><a href ="http://localhost:11288/2016/06/06/review-trigun/Default.aspx"><img src="../images/img3.jpg" title="Reseña: Trigun"></a></li>
                        </ul>
                        <!-- end Basic jQuery Slider -->

                    </div>
                    <!-- End outer wrapper -->

                    <script class="secret-source">

                        jQuery(document).ready(function ($) {

                            $('#banner-fade').bjqs({
                                animtype: 'slide',
                                height: 720,
                                width: 480,
                                responsive: true,
                                randomstart: true
                            });

                        });
                    </script>
                </div>
                <div class = "col-4 col-mm-3">				
                </div>
            </div>
        </div>
        <div id="sections" style="background-image: url(../images/pattern2.png); width: 100%;">
            <div class="row">
                <div class ="col-1-5 hide" ></div>
                <div class = "col-3 col-mm-4 col-m-12">                                     
                    <a href ="http://localhost:11288/Default.aspx?cat=1"><img class="section-img" src="../images/anime.jpg" alt></a>                       
                </div>
                <div class = "col-3 col-mm-4 col-m-12">                    
                    <a href ="http://localhost:11288/Default.aspx?cat=2"><img class="section-img" src="../images/tecnologia.jpg" alt></a>                    
                </div>
                <div class = "col-3 col-mm-4 col-m-12">                    
                    <a href ="http://localhost:11288/Default.aspx?cat=3"><img class="section-img" src="../images/videojuegos.jpg" alt></a>
                </div>
                <div class ="col-1-5 hide" ></div>
            </div>

        </div>
        <div id ="maincontent" style="background-image: url(../images/pattern3.png); width: 100%;">
            <div class = "row">
                <div class = "col-1 hide">				
                </div>
                <div class = "col-10 col-mm-12 col-m-12">
                    <table>
                        <tr>
                            <td>Id</td>
                            <td>Id Articulo</td>
                            <td>Id Usuario</td>
                            <td>Comentario</td>
                            <td>Fecha del comentario</td>
                        </tr>
                        <?php
                        $conn = pg_pconnect('host=localhost port=5432 dbname=anitechvgbd user=anitechvgadmin password=anitechvgadmin');
                        if (!$conn) {
                            echo "An error occurred.\n";
                            exit;
                        }
                        $query = "";
                        if($_SESSION['ust'] == 1){
                            $query = "SELECT * from comentarios";
                        } else {
                            $query = "SELECT * from comentarios where iduser = " . $_SESSION['usid'];
                        }
                        $result = pg_query($conn, $query);

                        if (!$result) {
                            echo "An error occurred.\n";
                            exit;
                        }

                        $arr = [];
                        $inc = 0;
                        while ($row = pg_fetch_row($result)) {
                            $id = $row[0];
                            $idart = $row[1];
                            $iduser = $row[2];
                            $com = $row[3];
                            $fecha = $row[4];
                            $jsonArrayObject = ["id" => $id, "idart" => $idart, "iduser" => $iduser, "comentario" => $com, "fechacoment" => $fecha];
                            $arr[$inc] = $jsonArrayObject;
                            $inc++;
                        
                        ?>
                        <tr>
                            <td><?= $row[0]?></td>
                            <td><?= $row[1]?></td>
                            <td><?= $row[2]?></td>
                            <td><?= $row[3]?></td>
                            <td><?= $row[4]?></td>                            
                            <td><form method="post" action ="http://localhost:8085/AniTechVgPHP/commentary/deleteComment.php">    
                                      <input type="hidden" name="id" value="<?= $row[0]?>"/>
                                    <input type="submit" value="Eliminar Comentario" />
                                </form></td>

                        </tr>
                        <?php
                        }
                        pg_close($conn);
                        
                        ?>
                    </table> 			
                </div>
                <div class = "col-1 hide">				
                </div>
            </div>

        </div>

        <footer class="footer-distributed">

            <div class="footer-left">

                <img class ="footer-img" src="../images/logofinal2.png"/>

                <p class="footer-links">
                    <a href="#">Home</a>
                    ·
                    <a href="#">Blog</a>                    
                    ·
                    <a href="#">About</a>
                    ·
                    <a href="#">Faq</a>
                    ·
                    <a href="#">Contact</a>
                </p>

                <p class="footer-company-name">AniTechVg &copy; 2015</p>
            </div>

            <div class="footer-center">

                <div>
                    <i class="fa fa-map-marker"></i>
                    <p><span>Av. Madero 255</span> Morelia, Mexico</p>
                </div>

                <div>
                    <i class="fa fa-phone"></i>
                    <p>+1 555 123456</p>
                </div>

                <div>
                    <i class="fa fa-envelope"></i>
                    <p><a href="mailto:support@anitech.vg">support@anitech.vg</a></p>
                </div>

            </div>

            <div class="footer-right">

                <p class="footer-company-about">
                    <img class ="footer-img" src="../images/contactanos.jpg" alt="contactanos"/>
                </p>

                <div class="footer-icons">

                    <a href="http://www.facebook.com/AniTechVG"><img src="../images/facebook64x64.png"/></a>
                    <a href="http://www.twitter.com/AniTechVG"><img src="../images/twitter64x64.png"/></a>
                    <a href="http://www.g.in/AniTechVG"><img src="../images/google64x64.png"/></a>


                </div>

            </div>

        </footer>

    </body>
</html>