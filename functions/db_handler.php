<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 */
class db_handler {

    private $conn;

    function __construct() {
        require_once getcwd() . '/functions/db_connect.php';
        
        // opening db connection
        $db = new db_connect();
        $this->conn = $db->connect();
        ini_set('mssql.charset', 'UTF-8');
    }


}

?>