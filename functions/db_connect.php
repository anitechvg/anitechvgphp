<?php
include_once 'psl-config.php';   // As functions.php is not included

class db_connect {
    private $conn;
 
    function __construct() {    
        
    }
 
    /**
     * Establishing database connection
     * @return database connection handler
     */
    function connect() {        
 
        // Connecting to mysql database
        $this->conn =new PDO('pgsql:dbname ='.DB.';host= ' .HOST. ';user= '.USER. ';password =' .PASSWORD);        
 
        // Check for database connection error
        if (!$this->conn) {
            die('Algo fue mal mientras se conectaba a MSSQL');
        }
 
        // returing connection resource
        return $this->conn;
    }
}